#!/usr/bin/env php
<?php

namespace PvPGNTracker\Binaries;

use \Memcached;

class Cache {

    const DEFAULT_TTL = 60;

    protected $memcache;

    public function __construct($servers, $timeout = 1, $tcp_nodelay = true) {
        $this->memcache = new Memcached();
        $this->memcache->setOption(Memcached::OPT_BINARY_PROTOCOL, true);
        $this->memcache->setOption(Memcached::OPT_TCP_NODELAY, $tcp_nodelay);
        $this->memcache->setOption(
            Memcached::OPT_CONNECT_TIMEOUT, $timeout * 1000
        );
        if (is_string($servers)) {
            $server = explode(":", $servers);
            $this->memcache->addServer($server[0], (int) $server[1]);
        } else if ($servers instanceof StdClass) {
            $this->memcache->addServer($server->hostname, $server->port);
        } else {
            foreach ($servers as $server) {
                $this->memcache->addServer($server->hostname, $server->port);
            }
        }
    }

    public function delete($key, $wait = 0) {
        return $this->memcache->delete($key, $wait);
    }

    public function get($key) {
        return $this->memcache->get($key);
    }

    public function set($key, $value, $ttl = self::DEFAULT_TTL) {
        if ($ttl < 1) {
            return $this->memcache->set($key, $value, 0);
        } else {
            return $this->memcache->set($key, $value, time() + $ttl);
        }
    }

}

$config = json_decode( file_get_contents( __DIR__ . '/../etc/config.json' ));

$cache = new Cache(
    $config->memcache->hostname . ':' . $config->memcache->port,
    3,
    true
);

$key_prefix = $config->memcache->key_prefix;
$key_suffix = $config->memcache->key_suffix;

$servers = array();
$server_uuids = array();

$rand = mt_rand( 0, 4000 );
while ( $rand > 0 ) {
    $servers[] = array(
        'uuid' => mt_rand(),
        'server_port' => 6112,
        'flags' => 0,
        'address' => 'pvpgn-' . mt_rand( 0, 100 ) . '.example.com',
        'server_location' => 'United States',
        'server_description' => 'Lorem ipsum dolor sit amet',
        'server_url' => 'http://' . mt_rand( 0, 100 ) . '.example.com/',
        'contact_name' => 'fakeUser.' . mt_rand( 0, 99 ),
        'contact_email' => 'fakeuser.' . mt_rand( 0, 99 ) . '@example.com',
        'uptime' => time() - 1527234263,
        'software' => 'NotAPvPGN',
        'version' => '0.0.1',
        'active_users' => mt_rand( 0, 10000 ),
        'active_channels' => mt_rand( 1, 100 ),
        'active_games' => mt_rand( 0, 10000 ),
        'total_games' => mt_rand( 0, 10000 ),
        'total_logins' => mt_rand( 0, 10000 ),
    );
    --$rand;
}

foreach ( $servers as $server ) {
    $cache->set(
        $key_prefix . 'server-' . $server[ 'uuid' ] . $key_suffix,
        serialize( $server ),
        900
    );
    $server_uuids[] = $server[ 'uuid' ];
}

$uuids = implode( ',', $server_uuids );

$cache->set( $key_prefix . 'servers' . $key_suffix, $uuids, 900 );

